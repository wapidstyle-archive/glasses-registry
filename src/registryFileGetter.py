# Saves a lot of hassle in testing.
# Kinda not necessary, but still saves a lot of hassle.

import platform
import os


def getRegistryFileWrite():
    if platform.system() == "Windows":
        if not os.path.exists("C:/Glasses"):
            os.makedirs("C:/Glasses/")
        return open("C:/Glasses/registry.reg", "w")
    elif platform.system() == "Linux":
        if not os.path.exists("/usr/share/glasses"):
            os.makedirs("/usr/share/glasses")
        return open("/usr/share/glasses/registry.reg", "w")
    else:
        return open("registry.reg", "w")

def getRegistryFileRead():
    if platform.system() == "Windows":
        if not os.path.exists("C:/Glasses"):
            os.makedirs("C:/Glasses/")
        return open("C:/Glasses/registry.reg", "r")
    elif platform.system() == "Linux":
        if not os.path.exists("/usr/share/glasses"):
            os.makedirs("/usr/share/glasses")
        return open("/usr/share/glasses/registry.reg", "r")
    else:
        return open("registry.reg", "r")
