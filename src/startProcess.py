#!/usr/bin/python3
import registryFileGetter
import registryCache
import registryUpdater


def replaceIfEmpty():
    print("Replacing if empty...")
    if (registryFileGetter.getRegistryFileRead().read() == "") or (registryFileGetter.getRegistryFileRead().read() == "\n"):
        file = registryFileGetter.getRegistryFileWrite().write(
            "[Glasses Registry 1.0]\n[End Of File]")

replaceIfEmpty()
registryCache.fillCache()
registryUpdater.startLoop()
