import registryCache


# High-level way to write to the registry
# Prevents access to functions in registryCache
def writeToRegistry(key, content):
    registryCache.setValueInCache(key, content)
