import registryFileGetter

cache = {}


def getCache():
    return cache


def setValueInCache(key, value):
    # TODO(wapidstyle) Govern the key, do whenever app permissions are added
    cache[key] = value


def writeCacheToFile():
    registry = registryFileGetter.getRegistryFileWrite()
    registry.write("[Glasses Registry 1.0]\n")
    for key in cache:
        registry.write(key + ": " + cache[key] + "\n")
    registry.write("[End Of File]\n")


def fillCache():
    print("Filling cache, please wait...")
    rawFile = registryFileGetter.getRegistryFileRead()
    contents = registryFileGetter.getRegistryFileRead().read().split("\n")
    print(contents)
    if (contents == ["[Glasses Registry 1.0]", "[End Of File]"]):
        contents = []
    else:
        del contents[len(contents) - 1]
        del contents[len(contents) - 1]
        del contents[0]

    print(contents)
    for entry in contents:
        print(entry)
        if not (entry == "[Glasses Registry 1.0]") or (entry == "[End Of File]"):
            line = entry.split(":")
            print("Key " + line[0] + " now is " + line[1])
            setValueInCache(line[0], line[1])
#
#setValueInCache("Testing/Boolean-True", "true")
#setValueInCache("Testing/Number", "21")
# writeCacheToFile()
