import registryCache


def readRegistryBool(key):
    if registryCache.getCache()[key] == "true":
        return True
    elif registryCache.getCache()[key] == "false":
        return False
    else:
        return None


def readRegistryString(key):
    return registryCache.getCache()[key]


def readRegistryNumber(key):
    try:
        return int(registryCache.getCache()[key])
    except KeyError:
        return None
