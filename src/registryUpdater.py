from threading import Thread
import time  # For time.sleep
import hashlib  # Hashing
import registryCache
import registryFileGetter


def updaterLoop():
    while True:
        print("[REGISTRY] Updating file from cache.")
        print("[REGISTRY] Constructing to-be file...")
        tobe = ""
        tobe += "[Glasses Registry 1.0]\n"
        for key in registryCache.getCache():
            tobe += key + ": " + registryCache.getCache()[key] + "\n"
        tobe += "[End Of File]\n"
        hashedCache = hashlib.sha256(tobe.encode("cp1252")).hexdigest()
        hashed = hashlib.sha256(
            registryFileGetter.getRegistryFileRead().read().encode("cp1252")).hexdigest()
        print("[REGISTRY] Cache is " + hashedCache)
        print("[REGISTRY] File is  " + hashed)
        if not hashed == hashedCache:
            # The file's out of date (or we assume, the cache should never
            # be out-of-date)
            regFile = registryFileGetter.getRegistryFileWrite()
            regFile.write(tobe)
            print("[REGISTRY] Updated.")
        else:
            print("[REGISTRY] Up to date")
        time.sleep(60)


def startLoop():
    Thread(target=updaterLoop).start()
